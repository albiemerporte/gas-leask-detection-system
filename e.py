from PyQt6.uic import loadUi
from PyQt6.QtWidgets import QApplication
from PyQt6.QtGui import QColor, QPixmap, QStandardItemModel, QStandardItem
from PyQt6.QtCore import QTimer
import pyqtgraph as pg
import random
from datetime import datetime

current_datetime = datetime.now()

class Main:
    def __init__(self):
        self.mainui = loadUi('monitoring v2.ui')
        self.mainui.show()
        
        self.mainui.chkSensor1.stateChanged.connect(self.sensor1trend)
        self.mainui.chkSensor2.stateChanged.connect(self.sensor2trend)
        self.mainui.chkSensor3.stateChanged.connect(self.sensor3trend)
        
        self.plot_widget = pg.PlotWidget()
        self.mainui.loVisual.addWidget(self.plot_widget)
        
        self.mainui.pbFs.clicked.connect(self.fs)
        self.mainui.pbExitFs.clicked.connect(self.exitfs)

        self.sensor1_activeness = [0]
        self.sensor2_activeness = [0]
        self.sensor3_activeness = [0]
        
        self.orangeline = QColor(255, 165, 0)
        self.yellowline = QColor(255, 255, 0)
        self.blue = QColor(20, 71, 224)

        self.timer = QTimer()
        self.timer.timeout.connect(self.chartrun)
        self.timer.start(100)

        pixmap = QPixmap("src/defaultpipe.jpg") 
        self.mainui.lblMainImageMap.setPixmap(pixmap)
        
        self.treemodel()
        
        for column in range(self.model.columnCount()):
            self.mainui.treeViewLog.resizeColumnToContents(column)
    
        self.mainui.pbExitFs.setVisible(False)
    
    def treemodel(self):
        self.model = QStandardItemModel()
        self.model.setHorizontalHeaderLabels(['Message For Detection Leak', 'Time', 'Date'])
        self.mainui.treeViewLog.setModel(self.model)
    
    def fs(self):
        self.mainui.showFullScreen()
        self.mainui.pbExitFs.setVisible(True)
        self.mainui.pbFs.setVisible(False)
        
    def exitfs(self):
        self.mainui.showNormal()
        self.mainui.pbExitFs.setVisible(False)
        self.mainui.pbFs.setVisible(True)
    
    def sensor1trend(self):
        length1 = len(self.sensor1_activeness) - 1
        
        if self.mainui.chkSensor1.isChecked():
            self.sensor1 = self.plot_widget.plot(self.sensor1_activeness, pen=self.yellowline, symbol='o', symbolSize=0, name='sense1')
        else:
            self.plot_widget.removeItem(self.sensor1)
        
        return length1

    def sensor2trend(self):
        length2 = len(self.sensor2_activeness) - 1
        
        if self.mainui.chkSensor2.isChecked():
            self.sensor2 = self.plot_widget.plot(self.sensor2_activeness, pen=self.orangeline, symbol='o', symbolSize=0, name='sense2')
        else:
            self.plot_widget.removeItem(self.sensor2)
        
        return length2
            
    def sensor3trend(self):
        length3 = len(self.sensor3_activeness) - 1
        
        if self.mainui.chkSensor3.isChecked():
            self.sensor3 = self.plot_widget.plot(self.sensor3_activeness, pen=self.blue, symbol='o', symbolSize=0, name='sense3')
        else:
            self.plot_widget.removeItem(self.sensor3)
        
        return length3

    def imghide(self):
        pixmaphide = QPixmap("src/defaultpipe.jpg") 
        self.mainui.lblMainImageMap.setPixmap(pixmaphide)
        
    def img1show(self):
        pixmap1show = QPixmap("src/pipe1.jpg") 
        self.mainui.lblMainImageMap.setPixmap(pixmap1show)
        
    def img2show(self):
        pixmap2show = QPixmap("src/pipe2.jpg") 
        self.mainui.lblMainImageMap.setPixmap(pixmap2show)
        
    def img3show(self):
        pixmap3show = QPixmap("src/pipe3.jpg") 
        self.mainui.lblMainImageMap.setPixmap(pixmap3show)
        
    def img12show(self):
        pixmap12show = QPixmap("src/pipe1and2.jpg") 
        self.mainui.lblMainImageMap.setPixmap(pixmap12show)
        
    def img23show(self):
        pixmap23show = QPixmap("src/pipe2and3.jpg") 
        self.mainui.lblMainImageMap.setPixmap(pixmap23show)
        
    def img13show(self):
        pixmap23show = QPixmap("src/pipe1and3.jpg") 
        self.mainui.lblMainImageMap.setPixmap(pixmap23show)
        
    def img123show(self):
        pixmap123show = QPixmap("src/pipe123.jpg") 
        self.mainui.lblMainImageMap.setPixmap(pixmap123show)

    def chartrun(self):
        
        self.input1 = float(self.mainui.cmbInput1.currentText())
        self.input2 = float(self.mainui.cmbInput2.currentText())
        self.input3 = float(self.mainui.cmbInput3.currentText())
        
        if self.mainui.chkSensor1.isChecked() or \
           self.mainui.chkSensor2.isChecked() or \
           self.mainui.chkSensor3.isChecked():
            self.plot_widget.clear()
            
        self.sensor1_activeness.append(random.uniform(0.0, self.input1))
        self.sensor1trend()
        
        self.sensor2_activeness.append(random.uniform(0.0, self.input2))
        self.sensor2trend()
        
        self.sensor3_activeness.append(random.uniform(0.0, self.input3))
        self.sensor3trend()
        
        if len(self.sensor1_activeness) == 50:
            self.sensor1_activeness = [self.sensor1_activeness[self.sensor1trend()]]
            self.sensor2_activeness = [self.sensor2_activeness[self.sensor2trend()]]
            self.sensor3_activeness = [self.sensor3_activeness[self.sensor3trend()]]
            self.plot_widget.clear()
            self.model.clear()
            self.treemodel()
            
        looklist1 = len(self.sensor1_activeness) - 1
        looklist2 = len(self.sensor2_activeness) - 1
        looklist3 = len(self.sensor3_activeness) - 1
        print(self.sensor1_activeness[looklist1])
        
        current_time = str(current_datetime.time())
        current_date = str(current_datetime.date())
        
        if self.sensor1_activeness[looklist1] == 0.0 and self.sensor2_activeness[looklist2] == 0.0 and self.sensor3_activeness[looklist3] == 0.0:
            detectionmsg1 = "Tube1 Leak Detected"
            log_records = [(detectionmsg1, current_time, current_date)]

            self.imghide()
        
        if self.sensor1_activeness[looklist1] > 0.1:
            detectionmsg1 = "Tube1 Leak Detected"
            log_records = [(detectionmsg1, current_time, current_date)]

            self.img1show()

            for record in log_records:
                message_item = QStandardItem(record[0])
                time_item = QStandardItem(record[1])
                date_item = QStandardItem(record[2])
                self.model.appendRow([message_item, time_item, date_item])        
        
        if self.sensor2_activeness[looklist2] > 0.1:
            detectionmsg2 = "Tube2 Leak Detected"
            log_records = [(detectionmsg2, current_time, current_date)]

            self.img2show()

            for record in log_records:
                message_item = QStandardItem(record[0])
                time_item = QStandardItem(record[1])
                date_item = QStandardItem(record[2])
                self.model.appendRow([message_item, time_item, date_item])
        
        if self.sensor3_activeness[looklist3] > 0.1:
            detectionmsg3 = "Tube3 Leak Detected"
            log_records = [(detectionmsg3, current_time, current_date)]

            self.img3show()

            for record in log_records:
                message_item = QStandardItem(record[0])
                time_item = QStandardItem(record[1])
                date_item = QStandardItem(record[2])
                self.model.appendRow([message_item, time_item, date_item])

        if self.sensor1_activeness[looklist1] > 0.1 and self.sensor2_activeness[looklist2] > 0.1:
            detectionmsg2 = "Tube1 and 2 Leak Detected"
            log_records = [(detectionmsg2, current_time, current_date)]

            self.img12show()

            for record in log_records:
                message_item = QStandardItem(record[0])
                time_item = QStandardItem(record[1])
                date_item = QStandardItem(record[2])
                self.model.appendRow([message_item, time_item, date_item])
                
        if self.sensor2_activeness[looklist2] > 0.1 and self.sensor3_activeness[looklist3] > 0.1:
            detectionmsg3 = "Tube2 and 3 Leak Detected"
            log_records = [(detectionmsg3, current_time, current_date)]

            self.img23show()

            for record in log_records:
                message_item = QStandardItem(record[0])
                time_item = QStandardItem(record[1])
                date_item = QStandardItem(record[2])
                self.model.appendRow([message_item, time_item, date_item])
                
        if self.sensor1_activeness[looklist1] > 0.1 and self.sensor3_activeness[looklist3] > 0.1:
            detectionmsg3 = "Tube1 and 3 Leak Detected"
            log_records = [(detectionmsg3, current_time, current_date)]

            self.img13show()

            for record in log_records:
                message_item = QStandardItem(record[0])
                time_item = QStandardItem(record[1])
                date_item = QStandardItem(record[2])
                self.model.appendRow([message_item, time_item, date_item])
                
        if self.sensor1_activeness[looklist1] > 0.1 and self.sensor2_activeness[looklist2] > 0.1 and self.sensor3_activeness[looklist3] > 0.1:
            detectionmsg3 = "Tube1, 2 and 3 Leak Detected"
            log_records = [(detectionmsg3, current_time, current_date)]

            self.img123show()

            for record in log_records:
                message_item = QStandardItem(record[0])
                time_item = QStandardItem(record[1])
                date_item = QStandardItem(record[2])
                self.model.appendRow([message_item, time_item, date_item])


if __name__ == '__main__':
    app = QApplication([])
    main = Main()
    app.exec()